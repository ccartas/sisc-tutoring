const request = require('supertest');
const app = require('./../app');
const $ = require('jquery');

jest.unmock('./../public/js/script');
import { displayJSON } from './../public/js/script';

describe('Testing Home Page', () => {
    test('Page should contain Heading Element', (done) => {
        request(app).get('/').then((response) => {
            document.body.innerHTML = response.text;
            expect($('h1').length).toBeTruthy();
            done();
        });
    });
    test('Heading Element text should be: Welcome to Homepage', (done) => {
       request(app).get('/').then((response) => {
        document.body.innerHTML = response.text;
        expect($('h1').text()).toEqual('Welcome to Homepage');
        done();
       });
    });
    test('Page should containe a logo loaded from assets directory: logo.png', (done) => {
       request(app).get('/').then((response) => {
         document.body.innerHTML = response.text;
         expect($('img').attr('src')).toEqual(expect.stringContaining('assets/logo.png'));
         done();
       });
    });
    test('Page should contain 2 anchors. Anchors should have the following ids [home, register] and the following content [Home, Register]', (done) => {
       request(app).get('/').then((response) => {
           document.body.innerHTML = response.text;
           expect($('#home').text()).toEqual('Home');
           expect($('#register').text()).toEqual('Register');
           done();
       });
    });
});

describe('Testing About Page', () => {
    test('Add a heading element with text Register Now', (done) => {
       request(app).get('/register.html').then((response) => {
           document.body.innerHTML = response.text;
           expect($('h1').text()).toEqual('Register Now')
           done();
       }) 
    });
    test('Register page should contain a form with 4 inputs for: Full Name, Email, Password and Confirm Password', (done) => {
       request(app).get('/register.html').then((response) => {
           document.body.innerHTML = response.text;
           expect($('form').length).toBeTruthy();
           expect($('input').length).toEqual(4);
           done();  
       });
    });
    test('Register form should contain a button with id="submit-btn" with text: Register', (done) => {
       request(app).get('/register.html').then((response) => {
          document.body.innerHTML = response.text;
          expect($('#submit-btn').text()).toEqual('Register');
          done();
       });
    });
})
describe('Testing displayJSON function', () => {
    test('Modify the displayJSON function to return parameters in JSON format with the following properties: fullName, email, password', (done) =>{
        expect(displayJSON('John', 'test@test.com', 'p@ss')).toBe(JSON.stringify({
            fullName: 'John',
            email: 'test@test.com',
            password: 'p@ss'
        }));
        done();
    });
    test('displayJSON should have default parameter value DEFAULT_VALUE', (done) => {
        expect(displayJSON('John', 'test@test.com')).toBe(JSON.stringify({
            fullName: 'John',
            email: 'test@test.com',
            password: 'DEFAULT_VALUE'
        }))
        done();
    })
    test('displayJSON should throw an error if fullName contains admin', (done) => {
         expect(() => {
                displayJSON('admin', 'band')
            }).toThrow();
            done();
    })
});

