# Subject 2
# Topic: JS

# Add the logic in the script tag to complete the following requirements
- On click event increase the value from span element;
- The variable used for managing the counter should not be attached to global object (window);